package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"os"
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "tfenv",
	Short: "A terraform environment management tool",
	Long: `A terraform environment management tool, that will
	enable you to download multiple versions of terraform and manage them within your shell environments`,
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Print the version number of tfenv",
	Long:  `All software has versions. This is tfenv's`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("tfenv version 1.0.0")
	},
}

func init() {
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	// rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.gokallinikos.yaml)")
	// rootCmd.PersistentFlags().BoolP("verbose", "v", false, "verbose mode")
	// rootCmd.PersistentFlags().StringVarP(&panoramaHost, "panorama-host", "l", "panorama.corp.jmfamily.com", "The Panorama Location")
	// rootCmd.PersistentFlags().StringVar(&panUsername, "panUsername", os.Getenv("PANORAMA_USER"), "username for the firewalls")
	// rootCmd.PersistentFlags().StringVar(&panPassword, "panPassword", os.Getenv("PANORAMA_PASSWORD"), "password for the firewalls")
	// rootCmd.PersistentFlags().StringVar(&awsUsername, "awsUsername", "", "username for the phoenix")
	// rootCmd.PersistentFlags().StringVar(&awsPassword, "awsPassword", "", "password for the phoenix")
	// rootCmd.PersistentFlags().StringVarP(&profile, "profile", "p", "lz-shared-services", "The AWS Profile for instance lookup")
	// rootCmd.PersistentFlags().StringVarP(&role, "role", "e", "ADMIN", "The AWS Profile for instance lookup")
	// rootCmd.PersistentFlags().StringVarP(&region, "region", "r", "us-east-1", "The AWS Profile for instance lookup")

	rootCmd.AddCommand(versionCmd)
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		// Search config in home directory with name ".gokallinikos" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigName(".tfenv")
	}

	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
	}

}
